# Introduction

This is a basic setup for the roman numeral conversion coding kata.

## Examples
1  I
4  IV
5  V
9  IX
10  X
40  XL
50  L
90  XC
99 XCIX
100  C
400  CD
500  D
900  CM
1000  M

## Requirements
* [Node](https://nodejs.org/en/)

## Usage
```npm install```
```npm test```
