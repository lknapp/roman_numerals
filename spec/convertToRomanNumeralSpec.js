var convertToRomanNumeral = require('../src/convertToRomanNumeral.js')

describe('#convertToRomanNumeral', function() {

  it('converts 1 to I', function() {
    expect(convertToRomanNumeral(1)).toEqual("I");
  });

  it('convert 2 to II', () => {
    expect(convertToRomanNumeral(2)).toEqual("II");
  });

  it('convert 5 to V', () => {
    expect(convertToRomanNumeral(5)).toEqual("V");
  });

  it('convert 4 to IV', () => {
    expect(convertToRomanNumeral(4)).toEqual("IV");
  });

  it('converts 6 to VI', () => {
    expect(convertToRomanNumeral(6)).toEqual("VI");
  });

  it('convert 9 to IX', () => {
    expect(convertToRomanNumeral(9)).toEqual("IX");
  });

  it('converts 15 to XV', () => {
    expect(convertToRomanNumeral(15)).toEqual("XV");
  });

  it('converts 99 to XCIX', () => {
    expect(convertToRomanNumeral(99)).toEqual("XCIX");
  });
});
