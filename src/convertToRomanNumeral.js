"use strict";

function convertToRomanNumeral(input) {
  const numeralArray = ["C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];
  const integerArray = [100, 90, 50, 40, 10, 9, 5, 4, 1];

  let output = "";
  while(input > 0) {
    integerArray.forEach((e, i) => {
      if (input >= e) {
        output += numeralArray[i];
        input -= e;
      }
    });
  };

  return output;
}

module.exports = convertToRomanNumeral;
